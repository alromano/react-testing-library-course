// import 'react-testing-library/cleanup-after-each';
// import 'jest-dom/extend-expect';
import React from 'react';
import { render, fireEvent } from 'react-testing-library';
import { FavoriteNumber } from '../favorite-number';

test('render it with react-testing-library', () => {
  const { getByLabelText, debug } = render(<FavoriteNumber />);
  const input = getByLabelText('Favorite Number');
  expect(input.value).toBe('0');
  debug();
});

test('firing a change updates the value', () => {
  const { getByLabelText, queryByTestId } = render(<FavoriteNumber />);
  const input = getByLabelText('Favorite Number');
  fireEvent.change(input, { target: { value: 3 } });
  expect(input.value).toBe('3');
  expect(queryByTestId('error-message')).toBe(null);
});

test('entering an invalid number shows an error message', () => {
  const { getByLabelText, getByTestId } = render(<FavoriteNumber />);
  const input = getByLabelText('Favorite Number');

  fireEvent.change(input, { target: { value: 11 } });

  expect(getByTestId('error-message').textContent).toBe('The number is invalid');
});
